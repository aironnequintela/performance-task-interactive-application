package com.ahehi.courtcounter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
private int a =0;
private int b =0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void displayPointA(int num)
    {
        TextView a =(TextView) findViewById(R.id.pointA);
        a.setText(""+num);
    }
    public void displayPointB(int num)
    {
        TextView b = (TextView) findViewById(R.id.pointB);
        b.setText(""+ num);
    }
    public void addThreeA(View view)
    {
        a+=3;
        displayPointA(a);
    }
    public void addTwoA(View view)
    {
        a+=2;
        displayPointA(a);
    }
    public void freeThrowA(View view)
    {
        a++;
        displayPointA(a);
    }

    public void addThreeB(View view)
    {
        b+=3;
        displayPointB(b);
    }
    public void addTwoB(View view)
    {
        b+=2;
        displayPointB(b);
    }
    public void freeThrowB(View view)
    {
        b++;
        displayPointB(b);
    }
    public void reset (View view)
    {
        a=0;
        b=0;
        displayPointA(a);
        displayPointB(b);

    }



}
